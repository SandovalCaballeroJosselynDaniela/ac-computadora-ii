ACTIVIDAD 2
Computadoras Electrónicas
```plantuml

@startmindmap
caption Sandoval Caballero Josselyn Daniela 18161244
title MAPA CONCEPTUAL uno1

*[#Orange] Computadoras Electrónicas 

**[#lightblue] En el siglo XX las computadoras, ayudaron y remplazaron las tareas manuales, la ingeniería alcanzó nuevos limites

**[#FFBBCC] 1944 IBM construyo la computadora electromecánica más grande LLAMADA “Harvard Mark I” 
***[#lemonChiffon] Construida para los aliados 
****[#MistyRose] Tenía 765,000 componentes
****[#MistyRose] 80 km de cable
****[#MistyRose] Con un eje de 15m 
****[#MistyRose] Utilizaba un motor con 5 caballos de fuerzas
****[#MistyRose] Tenía 3500 reles
***[#lemonChiffon] Usada para
****[#MistyRose] Hacer simulaciones para el proyecto Manhattan (La bomba atómica)
****[#MistyRose] Realizaba la multiplicación 15 segundos
****[#MistyRose] Realizaba la división dos segundos
****[#MistyRose] Realizaba la división dos segundos

**[#FFBBCC] Utilizaban Relé que era la piedra angular. compuesto por:
***[#Plum] Una bobina  
***[#Plum] Un brazo de hierro móvil
***[#Plum] Dos contactos
***[#lightgreen] Características 
***[#Plum] 1940- Un relé tenia la capacidad de cambiar 50 veces por segundo (50 Hertz)
***[#Plum] Los relés son mas susceptibles al daño por el uso y el tiempo.


**[#FFBBCC] Antecedentes
***[#lemonChiffon] 1947- En septiembre los operadores extrajeron una polilla, Grace Hopper dijo que cuando algo salía mal tenía bichos surgió el término “pop”
***[#lemonChiffon] 1904- John Ambrose Fleming desarrollo un componente llamado Válvula Termoiónica
***[#lemonChiffon] 1906- Lee De Forest agrego un tercer electrodo llamado “electrodo de control” los trio dodos fueron las bases para las radio y teléfonos
***[#lemonChiffon] 1941 Alan Turing creo the bombe diseñado para diseñar el código nazi 
***[#lemonChiffon] 1943- Colossu Mark I(conocida como la primera computadora Electrónica Programable) diseñada por Tommy Flowers fueron instaladas 1,600 los tubos de vacío 
***[#lemonChiffon] 1946- ENIAC diseñada por Jonh Mauchly y Presper Eckert fue la primera computadora programable de propósito general realizaba 5,000 sumas y restas de 10 dígitos por segundos
***[#lemonChiffon] 1947- El laboratorio Bell crearon el transistor
***[#lemonChiffon] 1957- Fue lanzada la IBM 608 la primera computadora comercial realizaba 4500 sumas y 80 divisiones o multiplicaciones por segundo

@endmindmap
```
Arquitectura de Computadoras
```plantuml
@startmindmap
caption Sandoval Caballero Josselyn Daniela 18161244
title MAPA CONCEPTUAL DOS.

*[#lightblue] Arquitectura de Computadoras

**[#Orange] Ley de Moore
***[#FFBBCC] Establece que la velocidad del procesador o el poder de procesamiento total de las computadoras se duplica cada doce meses
****[#lemonChiffon] Electrónica
*****[#MistyRose] El número de transistores por chip se duplica cada año
*****[#MistyRose] El costo del chip permanece sin cambios
*****[#MistyRose] Cada 18 meses se duplica la potencia
****[#lemonChiffon] Performance
*****[#MistyRose] Se incrementa la velocidad del procesador
*****[#MistyRose] Se incrementa la velocidad de memoria
*****[#MistyRose] La velocidad de la memoria corre siempre por detrás de la velocidad del procesador

**[#Orange] Funcionamiento de una Computadora.
 
***[#FFBBCC] Antes 
****[#lemonChiffon] La computadora eran sistemas cableados
****[#lemonChiffon] Tenia una entrada de datos
****[#lemonChiffon] Los cables tenia una secuencia de funciones 
***[#FFBBCC] Progrmación mediante Hardware
****[#lemonChiffon] Ahora
*****[#MistyRose]  La programacion mediante Software: en cada paso se realiza alguna operacion sobre los datos
*****[#MistyRose]  Utilización de transistores
*****[#MistyRose] Tenemos un interprete de instrucciones
*****[#MistyRose] Todas las funciones se realizan de manera interactiva
*****[#MistyRose] Progrmación mediante Hardware: cuando se requiere realizar otra tarea se debe cambiar el hardware

**[#Orange] Arquitectura Von Neumann
***[#lemonChiffon] Tiene 3 partes fundamentales
****[#Plum] CPU - Unidad Central de Procesamiento
****[#Plum] Memoria principal 
****[#Plum] Modulo de E/S
***[#lightgreen] Modelo
****[#Lavender] Descrita en 1945 por John Von Neumann
****[#Lavender] En su primer borrador de un informe EDVAC
****[#Lavender] Los datos y programas se almacenan en una misma memoria de lectura-escritura
****[#Lavender] Los contenidos de esta memoria se acceden indicando su posiciòn
****[#Lavender] Ejecuciòn en secuencia
****[#Lavender] Representaciòn binaria
***[#lightgreen] Antecedentes
****[#RosyBrown] Surgue el concepto "programa alamacenado"
*****[#Sienna] Se les concoe a las computadoras de ese tipo de modelo
****[#RosyBrown] La sepaciòn de la memoria y de la CPU acarreò un porblema denominado "Neumann bottleneck"
****[#Lavender]  Interconexiòn 
*****[#RosyBrown] Todo sus componenetes se comunican a travès de buses
******[#Coral] Su propòsito de los Buses es de reducir la cantidad de conexiones entre la CPU y sus sistemas
******[#Coral] Un Bus esta compuesto por
*******[#Sienna] Buses de Datos
*******[#Sienna] Buses de Direcciones
*******[#Sienna] Buses de Control

**[#Orange] Instrucciones
***[#FFBBCC]  La funcion de una computadora es la ejecuciòn de programas. Los progrmas se encuentran localizados en memoria y consiste de intrucciones
***[#FFBBCC] Las instrucciones consisten de secuencias de 1 y 0 (binarias) 
****[#Sienna] Llamadas còdigo de màquina
**[#Orange] Ciclo de ejecuciòn
***[#FFBBCC] UC obtiene la instrucciòn de memoria
***[#FFBBCC] Se incrementa el PC
***[#FFBBCC] La intrucciòn es decodificada
***[#FFBBCC] Se obtiene los operando requeridos por la instrucciòn
***[#FFBBCC] Se ejecuta y deja los resultados en registros o en memoria

**[#Orange] Arquitectura Harvard computadora  
***[#lightgreen]  Modelo
****[#Lavender] Originalmente se referia a las ARQUITECTURAS DE COMPUTADORAS que utilizaban dispositivos de almacenamiento fisicamente separados para las instrucciones y para los datos

***[#FFBBCC]  Contiene
****[#Lavender]  Este describe una arquitectura de diseño para un computador digital electrònico con partes que consistian en 
*****[#Coral] Unidad Central de Procesamiento
******[#Coral] Memoria Principal
******[#Coral] Sitemas de E/S
****[#Lavender] Memorias
*****[#Coral] Se podrian fabricar memorias mucho mas ràpidas pero tiene un costo alto y suge memoria CACHÈ
****[#Lavender] Soluciòn Harvard
*****[#Coral] Las instrucciones y los datos se almacenan en cachès separadas para mejorar el rendimiento
****[#Lavender] Voy a tener en la arquitectura 3 memorias
*****[#Coral] Procesador
*****[#Coral] Memoria de instrucciones
*****[#Coral] Meoria de Datos
*****[#Coral] Se utiliza comunmente en desarrollo de productos o sistemas de propósito específico


@endmindmap
```
Basura Electrónica
```plantuml
@startmindmap
caption Sandoval Caballero Josselyn Daniela 18161244
title MAPA CONCEPTUAL TRES.
*[#lightblue] Basura Electrónica
**[#lemonChiffon] Sele conoce a la chatarra electrónica como RAEE 
**[#lemonChiffon] Plantas de tratamiento
***[#lemonChiffon] "e-end"
****[#lemonChiffon] Esta basura es acomulada en el "e-end" ubucado en Estados Unidos
****[#FFBBCC] De esta basura ellos extren Oro, Cobre, PLata y Colbato
****[#FFBBCC] En e-end, borran la infromacion y destruyen los discos de las PC de forma segura
***[#FFBBCC] También se extraen datos, o reparación de dispositivos para robar información 
***[#FFBBCC] Muchas veces la basura electrónica no es reciclada de manera segura
***[#FFBBCC] Mucha parte de la basura electronica es exportada de manera ilegal
****[#FFBBCC] Los residuos contaminan el AGUA, LA TIERRA Y EL AIRE
****[#FFBBCC] CHINA, INDIA Y MÉXICO
***[#FFBBCC] Reciclar los porcesos de manera seguro es muy costoso

***[#lemonChiffon] Peru green recycling
****[#FFBBCC] Es una planata donde se le da tratamiento a los residuos electrónicos

***[#lemonChiffon] Techemet México
****[#FFBBCC] Recicla los componentes y los clasifica y se les paga a los clientes por el componenete reciclado
****[#FFBBCC] En México anualmente se desechan 150 y 180 mil basura electrónica
***[#lemonChiffon] Servicios Ecológicos de Costa Rica
****[#FFBBCC] Se basa en dar empleos y esta en diversos programas para reciclaje
****[#FFBBCC] Los clasifica, demantelamiento y de selección



**[#lemonChiffon] Efectos en la salud
***[#FFBBCC] Extraer los recursos de forma no adecuada son nocivos para la salud que que repiran componentes tóxicos
***[#FFBBCC] La placas las clafica por grandos de alto y bajo. Para posteiormente recuperar los componenetes primarios para ser reciclados


**[#lemonChiffon] Los dipositivos electrónicos problemática
***[#FFBBCC] Fueron diseñados para se desechados
***[#FFBBCC] Se convertira en una gran plaga del siglo XXI



***[#FFBBCC] Son ya un porblema ambiental
***[#FFBBCC] Los cambios tecnológicos
***[#FFBBCC] El Ministerio de AMbiente estima que en Perú hay 400 milo toneladas de basura electrónica
***[#FFBBCC] Un dispositvo tiene un rango de vida entre 3 a 5 años 

**[#lemonChiffon] Leyes en seguridad de los datos
***[#FFBBCC] 2016 la unión Europea implemento que cada pais recoja 45 toneladas por cada 100 toneladas a la venta de residuos
***[#FFBBCC] 2012 Se aprobo el reglamento pra la gestión y manejo de los RAEE
**[#lemonChiffon] Especialista en la problematica
***[#FFBBCC] Sonia Araníbal 
****[#FFBBCC] RAEE Son los residuos de paratos eléctricos y electronicos que ya han culminado su vida
***[#FFBBCC] Juan Daniel Ayala
****[#FFBBCC] Todo aquel aparato que termin+o su vida util es un aparato electrónico que se puede reciclar

**[#lemonChiffon] Transformación del los residuos
***[#FFBBCC] Nuevos componentes de para una futura PC
***[#FFBBCC] Elaboración de suela de zapatos
***[#FFBBCC] Tapas de las llantas de los carros
***[#FFBBCC] Los adornos de los feretros
***[#FFBBCC] Elaboración de adoquín


@endmindmap


```